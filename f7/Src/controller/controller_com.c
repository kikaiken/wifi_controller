/*
 * dynamixel_com.c
 *
 *  Created on: 2019/03/26
 *      Author: naoki
 */

#include "controller/controller_com.h"
#include "controller/controller_ring.h"
#include "main.h"
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

struct controller_ring_buf con_com_ring_rx;

uint8_t controller_com_data[6];
int controller_com_timeout_cnt = 0;

void controller_com_init(UART_HandleTypeDef *huart){
    controller_ring_init(&con_com_ring_rx, huart);

    HAL_UART_Receive_DMA(con_com_ring_rx.huart, con_com_ring_rx.buf, con_com_ring_rx.buf_size);
    memset(controller_com_data, 0, 6);
    HAL_Delay(10);
}

void controller_com_proc(){
    static enum controller_com_recv_st st = CONTROLLER_COM_RECV_ST_WAIT_START_0;
    uint8_t tmp[1];
    static uint8_t data_tmp[6];

    while(controller_ring_available(&con_com_ring_rx) > 0){
        controller_ring_getc(&con_com_ring_rx, tmp);
        switch(st){
            case CONTROLLER_COM_RECV_ST_WAIT_START_0:
                if(tmp[0] == CONTROLLER_COM_START_0){
                    st = CONTROLLER_COM_RECV_ST_WAIT_START_1;
                }else{
                    st = CONTROLLER_COM_RECV_ST_WAIT_START_0;
                }
                break;
            case CONTROLLER_COM_RECV_ST_WAIT_START_1:
                if(tmp[0] == CONTROLLER_COM_START_1){
                    st = CONTROLLER_COM_RECV_ST_WAIT_DATA_0;
                }else{
                    st = CONTROLLER_COM_RECV_ST_WAIT_START_0;
                }
                break;
            case CONTROLLER_COM_RECV_ST_WAIT_DATA_0:
                data_tmp[0] = tmp[0];
                break;
            case CONTROLLER_COM_RECV_ST_WAIT_DATA_1:
                data_tmp[1] = tmp[0];
                break;
            case CONTROLLER_COM_RECV_ST_WAIT_DATA_2:
                data_tmp[2] = tmp[0];
                break;
            case CONTROLLER_COM_RECV_ST_WAIT_DATA_3:
                data_tmp[3] = tmp[0];
                break;
            case CONTROLLER_COM_RECV_ST_WAIT_DATA_4:
                data_tmp[4] = tmp[0];
                break;
            case CONTROLLER_COM_RECV_ST_WAIT_DATA_5:
                data_tmp[5] = tmp[0];
                break;
            case CONTROLLER_COM_RECV_ST_WAIT_END_0:
                if(tmp[0] == CONTROLLER_COM_END_0){
                    st = CONTROLLER_COM_RECV_ST_WAIT_END_1;
                }else{
                    st = CONTROLLER_COM_RECV_ST_WAIT_START_0;
                }
                break;
            case CONTROLLER_COM_RECV_ST_WAIT_END_1:
                if(tmp[0] == CONTROLLER_COM_END_0){
                    memcpy(controller_com_data, data_tmp, 6);
                    controller_com_timeout_cnt = 0;
                }
                st = CONTROLLER_COM_RECV_ST_WAIT_START_0;
                break;
            default:
                st = CONTROLLER_COM_RECV_ST_WAIT_START_0;
                break;
        }
    }

}

uint8_t* controller_com_get_data(){
    return controller_com_data;
}
//10ms timer
void controller_com_timer(){
    controller_com_timeout_cnt++;
    controller_com_proc();
    if(controller_com_timeout_cnt > 5){
        memset(controller_com_data, 0, 6);
    }
}
