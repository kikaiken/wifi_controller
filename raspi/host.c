#include "common.h"
//#define DEBUG

int main(){

  struct sockaddr_in bindaddr4;
  struct sockaddr_in client_addr4;
  memset(&bindaddr4, 0, sizeof(bindaddr4));
  memset(&client_addr4, 0, sizeof(client_addr4));

  int fd4 = -1;

  int status = -1;

  fd4 = socket(AF_INET, SOCK_DGRAM, 0);
  if( fd4 < 0 ){
    fprintf(stderr,"Failed to open IPv4 socket\n");
    exit(EXIT_FAILURE);
  }
  bindaddr4.sin_family = AF_INET;
  bindaddr4.sin_addr.s_addr = INADDR_ANY;
  bindaddr4.sin_port = htons(PORT);
  status = bind(fd4, (struct sockaddr *)&bindaddr4, sizeof(bindaddr4));
  
  if( fd4 < 0 || status < 0){
    fprintf(stderr,"Failed to bind IPv4 socket on %d\n", PORT);
    exit(EXIT_FAILURE);
  }

  uint8_t *recv_buf = (uint8_t *)malloc(RECV_BUFF_SIZE);
  if(!recv_buf){
    fprintf(stderr,"Failed to allocate recv_buf\n");
    exit(EXIT_FAILURE);
  }

  printf("Recv STANDBY\n");
  int recv_size = 0;
  
  socklen_t client_addr4_len = sizeof(client_addr4);
 #ifndef DEBUG
  int res = serial_init();
  if(res < 0) {
	close(fd4);
	exit(1);
  }
#endif
  while(1){
    recv_size = recvfrom(fd4, recv_buf, RECV_BUFF_SIZE, 0, (struct sockaddr *)&client_addr4, &client_addr4_len);
    if(recv_size != 6) continue;
    printf("Recv:");
    for(int i=0;i<recv_size;i++){
      printf("%02X ", recv_buf[i]);
    }
    printf("\n");
#ifndef DEBUG
    serial_controller_com_send(recv_buf);
#endif
  }
}
