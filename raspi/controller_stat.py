import time
import RPi.GPIO as GPIO
import os
import sys
import socket
import binascii

import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

import subprocess
import multi_server

# Raspberry Pi pin configuration:
RST = None     # on the PiOLED this pin isnt used
# Note the following are only used with SPI:
DC = 23
SPI_PORT = 0
SPI_DEVICE = 0

# Beaglebone Black pin configuration:
# RST = 'P9_12'
# Note the following are only used with SPI:
# DC = 'P9_15'
# SPI_PORT = 1
# SPI_DEVICE = 0

# 128x32 display with hardware I2C:
disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)

# Initialize library.
disp.begin()

# Clear display.
disp.clear()
disp.display()

# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
width = disp.width
height = disp.height

# Draw some shapes.
# First define some constants to allow easy resizing of shapes.
padding = -2
top = padding
bottom = height-padding
# Move left to right keeping track of the current x position for drawing shapes.
x = 0

# Load default font.
font = ImageFont.load_default()

## State setting
RASPI_STATE = 0
COMMAND = 1
HRSW = 2

## GPIO setting
PIN_LED1 = 19
PIN_LED2 = 26

PIN_BUTTON1 = 20 

## controller
CONTROLLER_LEFT     = 0b1000000000000000
CONTROLLER_DOWN     = 0b0100000000000000
CONTROLLER_RIGHT    = 0b0010000000000000
CONTROLLER_UP       = 0b0001000000000000
CONTROLLER_START    = 0b0000100000000000
CONTROLLER_RSTICK   = 0b0000010000000000
CONTROLLER_LSTICK   = 0b0000001000000000
CONTROLLER_SELECT   = 0b0000000100000000
CONTROLLER_SQUARE   = 0b0000000010000000
CONTROLLER_CROSS    = 0b0000000001000000
CONTROLLER_CIRCLE   = 0b0000000000100000
CONTROLLER_TRIANGLE = 0b0000000000010000
CONTROLLER_R1       = 0b0000000000001000
CONTROLLER_L1       = 0b0000000000000100
CONTROLLER_R2       = 0b0000000000000010
CONTROLLER_L2       = 0b0000000000000001

def ps2_controller_key_parser(key):
    button_data = (key[0]<<8) + key[1]
    direction = ""
    pushed_button = ""
    if button_data&CONTROLLER_LEFT:
        direction += "LEFT "
    else:
        direction += "     "
        
    if button_data&CONTROLLER_DOWN:
        direction += "DOWN "
    else:
        direction += "     "
        
    if button_data&CONTROLLER_RIGHT:
        direction += "RIGHT "
    else:
        direction += "      "

    if button_data&CONTROLLER_UP:
        direction += "UP "
    else:
        direction += "   "
        
    if button_data&CONTROLLER_SQUARE:
        pushed_button += "s "
    else:
        pushed_button += "  "

    if button_data&CONTROLLER_CROSS:
        pushed_button += "x "
    else:
        pushed_button += "  "

    if button_data&CONTROLLER_CIRCLE:
        pushed_button += "o "
    else:
        pushed_button += "  "

    if button_data&CONTROLLER_TRIANGLE:
        pushed_button += "t "
    else:
        pushed_button += "  "

    if button_data&CONTROLLER_R1:
        pushed_button += "R1 "
    else:
        pushed_button += "   "

    if button_data&CONTROLLER_L1:
        pushed_button += "L1 "
    else:
        pushed_button += "   "

    if button_data&CONTROLLER_R2:
        pushed_button += "R2 "
    else:
        pushed_button += "   "

    if button_data&CONTROLLER_L2:
        pushed_button += "L2 "
    else:
        pushed_button += "   "

    if button_data&CONTROLLER_RSTICK:
        pushed_button += "R3 "
    else:
        pushed_button += "   "

    if button_data&CONTROLLER_LSTICK:
        pushed_button += "L3 "
    else:
        pushed_button += "   "

    return direction, pushed_button


## class
class LCD_Controller:
    def __init__(self, socket_path):
        self.state = RASPI_STATE
        self.pre_state = RASPI_STATE
        self.image = Image.new('1', (width, height))
        self.draw = ImageDraw.Draw(self.image)
        self.draw.rectangle((0,0,width,height), outline=0, fill=0)
        
        self.unix_server = multi_server.MultiServer(socket_path)


    def setup(self):
        self.unix_server.setup()


    def switch_callback(self, gpio_pin):
        if GPIO.input(PIN_BUTTON1) == GPIO.LOW:
            GPIO.output(PIN_LED1, GPIO.HIGH)
            print "Pushed"
            if self.state is HRSW:
                self.state = RASPI_STATE 
            elif self.state is COMMAND:
                self.state = HRSW
            elif self.state is RASPI_STATE:
                self.state = COMMAND


    def Draw_raspiState(self):
        # Shell scripts for system monitoring from here : https://unix.stackexchange.com/questions/119126/command-to-display-memory-usage-disk-usage-and-cpu-load
        cmd = "hostname -I | cut -d\' \' -f1"
        IP = subprocess.check_output(cmd, shell = True )
        cmd = "top -bn1 | grep load | awk '{printf \"CPU Load: %.2f\", $(NF-2)}'"
        CPU = subprocess.check_output(cmd, shell = True )
        cmd = "free -m | awk 'NR==2{printf \"Mem: %s/%sMB %.2f%%\", $3,$2,$3*100/$2 }'"
        MemUsage = subprocess.check_output(cmd, shell = True )
        cmd = "df -h | awk '$NF==\"/\"{printf \"Disk: %d/%dGB %s\", $3,$2,$5}'"
        Disk = subprocess.check_output(cmd, shell = True )
    
        cmd = "nmcli -f SSID,ACTIVE,SIGNAL dev wifi list | grep yes | awk '{print $1} {print $3}'"
        Wifi = subprocess.check_output(cmd, shell = True ).splitlines()
        if(len(Wifi) != 0):
            Wifi = "SSID: "+str(Wifi[0])
            self.draw.text((x, top+16),    Wifi,  font=font, fill=255)
        # Write two lines of text.
    
        #cmd = "ping 192.168.0.88 -c 1 | awk '{print $7}' | grep \"time\" | sed -e \"s/time=//\""
        #Ping = subprocess.check_output(cmd, shell=True)
    
        self.draw.text((x, top),       "IP: " + str(IP),  font=font, fill=255)
        self.draw.text((x, top+8),     str(CPU), font=font, fill=255)
        #self.draw.text((x, top+25),    "Ping: "+str(Ping).strip()+"ms",  font=font, fill=255)
        self.pre_state = RASPI_STATE
    
    def Draw_Command(self):
        self.draw.text((x, top), "Received Commend",  font=font, fill=255)
        text = self.unix_server.get_message()
        if text:
            text = text[:12]
            direction, button = ps2_controller_key_parser(text)
            self.draw.text((x+2, top+8), binascii.hexlify(text)[:12],  font=font, fill=255)
            self.draw.text((x, top+16), direction, font=font, fill=255)
            self.draw.text((x, top+24), button, font=font, fill=255)
        else:
            self.draw.text((x+2, top+8), "None",  font=font, fill=255)

        self.pre_state = COMMAND

            
    
    def Draw_HRSW(self):
        self.image = Image.open('gipnoza.ppm').convert('1')
        self.pre_state = HRSW

    def Draw_LCD(self):
            if(self.pre_state is self.state):
                return
            self.unix_server.run()
            if self.pre_state is HRSW and self.state is not HRSW:
                self.image = Image.new('1', (width, height))
                self.draw = ImageDraw.Draw(self.image)
                
            self.draw.rectangle((0,0,width,height), outline=0, fill=0)

            if self.state is RASPI_STATE:
                self.Draw_raspiState()
            elif self.state is COMMAND:
                self.Draw_Command()
            elif self.state is HRSW:
                self.Draw_HRSW()

            disp.image(self.image)
            disp.display()

    def close(self):
        self.unix_server.close()

    


def main():
    lcd_con = LCD_Controller('/tmp/command_port.sock')
    GPIO.setmode(GPIO.BCM)
    
    GPIO.setup(PIN_BUTTON1, GPIO.IN)
    GPIO.setup(PIN_LED1, GPIO.OUT)
    GPIO.add_event_detect(PIN_BUTTON1, GPIO.FALLING, bouncetime=200)
    GPIO.add_event_callback(PIN_BUTTON1, lcd_con.switch_callback)
    lcd_con.setup()
    
    lcd_con.Draw_LCD()
    try:
        while True:
    
            # Draw a black filled box to clear the image.
    
    
            # Display image.
    
            GPIO.output(PIN_LED1, GPIO.LOW)
            lcd_con.Draw_LCD();
            #GPIO.output(PIN_LED2, GPIO.LOW)
            time.sleep(0.1)
    
    finally:
        #GPIO.cleenup()
        lcd_con.close()


if __name__ == "__main__":
    main()

