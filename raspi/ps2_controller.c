/*
 * SPI testing utility (using spidev driver)
 *
 * Copyright (c) 2007  MontaVista Software, Inc.
 * Copyright (c) 2007  Anton Vorontsov <avorontsov@ru.mvista.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * Cross-compile with cross-gcc -I/path/to/cross-kernel/include
 */

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <wiringPi.h>

#include "common.h"

#define CS_PIN 6
#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

int fd;
static void pabort(const char *s)
{
	perror(s);
	abort();
}

static const char *device = "/dev/spidev0.0";
static uint8_t mode;
static uint8_t bits = 8;
static uint32_t speed = 60000;
static uint16_t delay_spi = 100;

static uint8_t msb2lsb(uint8_t data_msb);

static void transfer()
{

	digitalWrite(CS_PIN, 0);
	int ret;
	uint8_t tx[] = {
		0x80, 0x42, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00,
	};
	uint8_t rx[ARRAY_SIZE(tx)] = {0, };
	struct spi_ioc_transfer tr = {
		.tx_buf = (unsigned long)tx,
		.rx_buf = (unsigned long)rx,
		.len = ARRAY_SIZE(tx),
		.delay_usecs = delay_spi,
		.speed_hz = speed,
		.bits_per_word = bits,
	};

	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
	if (ret < 1)
		pabort("can't send spi message");

	for (ret = 0; ret < ARRAY_SIZE(tx); ret++) {
		if (!(ret % 6))
			puts("");
		printf("%.2X ", rx[ret]);
	}
	puts("");
	digitalWrite(CS_PIN, 1);

}


static int spi_rxtx(uint8_t tx, uint8_t* rx){
	int ret;
	struct spi_ioc_transfer tr = {
		.tx_buf = (unsigned long)&tx,
		.rx_buf = (unsigned long)rx,
		.len = 1,
		.delay_usecs = 1,
		.speed_hz = speed,
		.bits_per_word = bits,
	};
	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
	if (ret < 1){
		pabort("can't send spi message");
		return ret;
	}

}

static void spi_enable(){
	digitalWrite(CS_PIN, 0);
}

static void spi_disable(){
	digitalWrite(CS_PIN, 1);
}

static void spi_wait(){
	volatile int i=100;
	while(i-->0);
}

int ps2_controller_get_data(uint8_t *data){
	uint8_t tx[10] = {
		0x80, 0x42, 0x00, 0x00, 0x00, 
		0x00, 0x00, 0x00, 0x00, 0x00,
	};
	uint8_t rx[10] = {0};

	spi_enable();
	spi_wait();
	for(int i=0;i<3;i++){
		spi_rxtx(tx[i], &rx[i]);
		spi_wait();
	}
	for(int i=3;i<9;i++){
		spi_rxtx(tx[i], &rx[i]);
		spi_wait();
	}
	spi_disable();
	data[0] = msb2lsb(~rx[3]&0xFF);
	data[1] = msb2lsb(~rx[4]&0xFF);
  	data[2] = msb2lsb(rx[5]);
  	data[3] = msb2lsb(rx[6]);
  	data[4] = msb2lsb(rx[7]);
  	data[5] = msb2lsb(rx[8]);
	return 6;

}

int ps2_controller_init()
{
	if(wiringPiSetup() == -1) return 1;
	printf("setupOK\n");

	pinMode(CS_PIN, OUTPUT);
	digitalWrite(CS_PIN, 1);
	int ret = 0;
	
	mode = SPI_CPHA | SPI_CPOL; //clock phase
	
	fd = open(device, O_RDWR);
	if (fd < 0)
		pabort("can't open device");

	/*
	 * spi mode
	 */
	ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
		pabort("can't set spi mode");

	ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
	if (ret == -1)
		pabort("can't get spi mode");

	/*
	 * bits per word
	 */
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
		pabort("can't set bits per word");

	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
	if (ret == -1)
		pabort("can't get bits per word");

	/*
	 * max speed hz
	 */
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		pabort("can't set max speed hz");

	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		pabort("can't get max speed hz");

	printf("spi mode: %d\n", mode);
	printf("bits per word: %d\n", bits);
	printf("max speed: %d Hz (%d KHz)\n", speed, speed/1000);
	return 0;
}

/*
 * key[0]: cross keys
 * key[1]: fig keys
 */
void ps2_controller_key_parser(uint8_t* key){
    printf("Key:");
    uint16_t button_data = (key[0]<<8)+key[1];

    if(button_data&CONTROLLER_LEFT){
        printf("LEFT ");
    }else{
        printf("     ");
    }

    if(button_data&CONTROLLER_DOWN){
        printf("DOWN ");
    }else{
        printf("     ");
    }

    if(button_data&CONTROLLER_RIGHT){
        printf("RIGHT ");
    }else{
        printf("      ");
    }

    if(button_data&CONTROLLER_UP){
        printf("UP ");
    }else{
        printf("   ");
    }

    if(button_data&CONTROLLER_SQUARE){
        printf("SQUARE ");
    }else{
        printf("       ");
    }

    if(button_data&CONTROLLER_CROSS){
	printf("CROSS ");
    }else{
	printf("      ");
    }

    if(button_data&CONTROLLER_CIRCLE){
        printf("CIRCLE ");
    }else{
        printf("       ");
    }

    if(button_data&CONTROLLER_TRIANGLE){
        printf("TRIANGLE ");
    }else{
        printf("         ");
    }

    if(button_data&CONTROLLER_R1){
        printf("R1 ");
    }else{
        printf("   ");
    }

    if(button_data&CONTROLLER_L1){
        printf("L1 ");
    }else{
        printf("   ");
    }

    if(button_data&CONTROLLER_R2){
        printf("R2 ");
    }else{
        printf("   ");
    }

    if(button_data&CONTROLLER_L2){
        printf("L2 ");
    }else{
        printf("   ");
    }

    if(button_data&CONTROLLER_RSTICK){
        printf("R3 ");
    }else{
        printf("   ");
    }

    if(button_data&CONTROLLER_LSTICK){
        printf("L3 ");
    }else{
        printf("   ");
    }

    printf("\n");
}

static uint8_t msb2lsb(uint8_t data_msb){
    uint8_t data_lsb=0;
    data_lsb += ((data_msb>>7)&0b00000001)<<0;
    data_lsb += ((data_msb>>6)&0b00000001)<<1;
    data_lsb += ((data_msb>>5)&0b00000001)<<2;
    data_lsb += ((data_msb>>4)&0b00000001)<<3;
    data_lsb += ((data_msb>>3)&0b00000001)<<4;
    data_lsb += ((data_msb>>2)&0b00000001)<<5;
    data_lsb += ((data_msb>>1)&0b00000001)<<6;
    data_lsb += ((data_msb>>0)&0b00000001)<<7;

    return data_lsb;
}
