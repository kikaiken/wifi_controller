#!/usr/bin/env python

import os
import sys
import socket
import threading
import select
import struct
import binascii

class MultiServer:
    def __init__(self, socket_path):
        self.socket_path = socket_path
        self.read_waiters = {}
        self.write_waiters = {}
        self.connections = {}
        self.message= ""

    def setup(self):
        self.ssock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.ssock.setblocking(False)
        self.ssock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
        try:
            self.ssock.bind(self.socket_path)
        except socket.error as msg:
            self.ssock.close()
            print 'Bind fallido. Error Code : ' + str(msg[0]) + 'Message' + msg[1]
            sys.exit()

        self.ssock.listen(1)

        self.read_waiters[self.ssock.fileno()] = (self.accept_handler, ())


    def run(self):
        rlist, wlist, _ = select.select(self.read_waiters.keys(), self.write_waiters.keys(), [], 0)
        #print rlist
        for r_fileno in rlist:
            handler, args = self.read_waiters.pop(r_fileno)
            handler(*args)

        ##for w_fileno in wlist:
        ##    handler, args = self.write_waiters.pop(w_fileno)
        ##    handler(*args)

    # accept handler
    def accept_handler(self):
        csock, c_address = self.ssock.accept()
        csock.setblocking(False)
        #sys.stdout.write('New client: {0}'.format(c_address))
        
        self.connections[csock.fileno()] = (csock, c_address)

        self.read_waiters[csock.fileno()] = (self.recv_handler, (csock.fileno(), ))
        self.read_waiters[self.ssock.fileno()] = (self.accept_handler, ())
    
    # recv handler
    def recv_handler(self, fileno):
        def terminate():
            del self.connections[csock.fileno()]
            csock.close()
            #sys.stdout.write('Bye-Bye: {0}'.format(c_address))

        csock, c_address = self.connections[fileno]

        try:
            self.message = csock.recv(1024)
        except OSError:
            terminate()
            return

        if len(self.message) == 0:
            terminate()
            return

        self.message = bytearray(self.message)

        sys.stdout.write('Recv: {0} to {1}\n'.format(self.message, c_address))
        
        self.read_waiters[csock.fileno()] = (self.recv_handler, (csock.fileno(), ))
        #self.write_waiters[fileno] = (self.send_handler, (fileno, message))

    # send handler
    def send_handler(self, fileno, message):
        csock, c_address = self.connections[fileno]

        sent_len = csock.send(message)
        #sys.stdout.write('Send: {0} to {1}'.format(message[:sent_len].decode(), c_address))

        if sent_len == len(message):
            self.read_waiters[csock.fileno()] = (self.recv_handler, (csock.fileno(), ))

        else:
            self.write_waiters[fileno] = (self.send_handler, (fileno, message[sent_len:]))

    def get_message(self):
        return self.message

    def close(self):
        os.remove(self.socket_path)
            


def main():
    server = MultiServer('/tmp/myapp.sock')

    try:
        server.setup()
        print "setup"
        while True:
            server.run()
    except KeyboardInterrupt :
        sys.stdout.write('AHHHHHHHH!\n')

    finally :
        os.remove(server.socket_path)
        
        


if __name__ == '__main__':
    main()

