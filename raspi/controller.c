#include "common.h"


//#define DEBUG
int main(){

  size_t data_size = 0;

  int sock;
  int unix_socket = -1;

  int ret_code = -1;
  int flags = 0;

  struct sockaddr_in addr;
  struct sockaddr_in sender_info;

  struct sockaddr_un unix_addr;
  memset(&unix_addr, 0, sizeof(unix_addr));

  //Create UDP Socket
  sock = socket(AF_INET,SOCK_DGRAM,0);
  if(sock < 0){
    perror("Failed to create socket hogehoge");
    return -1;
  }

  //Bind socket to recieve packet
  addr.sin_family = AF_INET;
  addr.sin_port = htons(PORT);
  addr.sin_addr.s_addr = INADDR_ANY;
  bind(sock,(struct sockaddr *)&addr,sizeof(addr));

  //Setting Destination Addresses
  sender_info.sin_family = AF_INET;
  sender_info.sin_port = htons(PORT);
  sender_info.sin_addr.s_addr = inet_addr("192.168.0.88");

  // Create UNIX domain socket
  unix_socket = socket(AF_LOCAL, SOCK_STREAM, 0);
  if(unix_socket == -1){
    perror("Failed to create unix domain socket");
    return -1;
  }

  // Setting socket path
  unix_addr.sun_family = AF_LOCAL;
  strcpy(unix_addr.sun_path, UNIX_SOCK_PATH);

  // connection
  ret_code = connect(unix_socket, (const struct sockaddr *)&unix_addr, sizeof(unix_addr));
  if(ret_code == -1){
    perror("Failed to create socket");
    close(unix_socket);
    return -1;
  }

  //Allocate Buffer
  unsigned char *send_buf = (unsigned char*)malloc(SEND_BUFF_SIZE);
  if(send_buf == NULL){
    perror("Failed to allocate buffer");
    return -1;
  }
  size_t send_size;

  ps2_controller_init();

  while(1){
    send_size = ps2_controller_get_data(send_buf);
#ifndef DEBUG
    if(sendto(sock,send_buf,send_size,0,(struct sockaddr *)&sender_info,sizeof(sender_info)) < 0){
      perror("Faild to send message");
      return -1;
    }
#endif
    // unix domain socket への送信
    if(send(unix_socket, send_buf, send_size, flags) < 0) {
      perror("Failed to send message");
      close(unix_socket);
      return -1;
    }
#ifdef DEBUG
    ps2_controller_key_parser(send_buf);
#endif
    printf("Send:");
    for(int i=0; i<6; i++){
      printf("%02X ", send_buf[i]);
    }
    printf("\n");
    usleep(10000);
  }

}
