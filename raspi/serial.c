#include "common.h"
#define SERIAL_PORT "/dev/ttyUSB0"

#define CONTROLLER_COM_START_0 0xFF
#define CONTROLLER_COM_START_1 0xFE

#define CONTROLLER_COM_END_0 0x00
#define CONTROLLER_COM_END_1 0x01

int serial_fd;                             // ファイルディスクリプタ
int serial_init(){
    struct termios tio;                 // シリアル通信設定
    int baudRate = B460800;

    serial_fd = open(SERIAL_PORT, O_RDWR);     // デバイスをオープンする
    if (serial_fd < 0) {
        printf("open error\n");
        exit(1);
    }
    tio.c_cflag += CREAD;               // 受信有効
    tio.c_cflag += CLOCAL;              // ローカルライン（モデム制御なし）
    tio.c_cflag += CS8;                 // データビット:8bit
    tio.c_cflag += 0;                   // ストップビット:1bit
    tio.c_cflag += 0;                   // パリティ:None
    cfsetispeed( &tio, baudRate );
    cfsetospeed( &tio, baudRate );
    cfmakeraw(&tio);                    // RAWモード
    tcsetattr( serial_fd, TCSANOW, &tio );     // デバイスに設定を行う
    ioctl(serial_fd, TCSETS, &tio);            // ポートの設定を有効にする
}

int serial_controller_com_send(uint8_t *send_data){
    uint8_t buf[10];
    buf[0] = 0xFF;
    buf[1] = 0xFE;
    buf[8] = 0x00;
    buf[9] = 0x01;
    memcpy(&buf[2], send_data, 6);
    serial_send(buf, 10);
}

int serial_send(uint8_t *send_data, int len){
    int sent_len = write(serial_fd, send_data, len);
    if(sent_len != len){
	    printf("Failed to Send\n");
    }else{
	    printf("OK\n");
    }
}

int serial_recv(uint8_t *recv_data, int len){
    return read(serial_fd, recv_data, len);
}
