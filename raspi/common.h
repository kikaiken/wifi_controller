#ifndef COMMON_H
#define COMMON_H

#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <arpa/tftp.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>

#define RECV_BUFF_SIZE 1024
#define SEND_BUFF_SIZE 1024

#define PORT 7777
enum {
	CONTROLLER_LEFT     = 0b1000000000000000,
	CONTROLLER_DOWN     = 0b0100000000000000,
	CONTROLLER_RIGHT    = 0b0010000000000000,
	CONTROLLER_UP       = 0b0001000000000000,
	CONTROLLER_START    = 0b0000100000000000,
	CONTROLLER_RSTICK   = 0b0000010000000000,
	CONTROLLER_LSTICK   = 0b0000001000000000,
	CONTROLLER_SELECT   = 0b0000000100000000,
	CONTROLLER_SQUARE   = 0b0000000010000000,
	CONTROLLER_CROSS    = 0b0000000001000000,
	CONTROLLER_CIRCLE   = 0b0000000000100000,
	CONTROLLER_TRIANGLE = 0b0000000000010000,
	CONTROLLER_R1       = 0b0000000000001000,
	CONTROLLER_L1       = 0b0000000000000100,
	CONTROLLER_R2       = 0b0000000000000010,
	CONTROLLER_L2       = 0b0000000000000001,
};
int ps2_controller_init();
int ps2_controller_get_data(uint8_t *data);
void ps2_controller_key_parser(uint8_t* key);

int serial_init();
int serial_controller_com_send(uint8_t *send_data);
int serial_send(uint8_t *send_data, int len);
int serial_recv(uint8_t *send_data, int len);
#endif
